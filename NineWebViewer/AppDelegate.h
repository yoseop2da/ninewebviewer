//
//  AppDelegate.h
//  NineWebViewer
//
//  Created by Apposter MacBook 2 on 2014. 8. 1..
//  Copyright (c) 2014년 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
